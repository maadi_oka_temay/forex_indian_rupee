import pandas as pd
import sys
import numpy as np
from sklearn.linear_model import LinearRegression
import matplotlib.pyplot as plt
import datetime as dt
import matplotlib
import matplotlib.dates as mdates
from statsmodels.tsa.arima_model import ARIMA


class PolyRegTest:
	
	def __init__(self):
		__file_name__ = '../data/train/USD-INR_1997-2016.csv'
		self.df = pd.read_csv(__file_name__)
		
	def visualize(self):
		y = list(self.df['Price'])
		init_val = y[0]
		plt.ylabel('USD $/ 1 INR')
		#	locator.MAXTICKS = 10000
		plt.gca().xaxis.set_major_formatter(mdates.DateFormatter('%d-%m-%Y'))
		plt.gca().xaxis.set_major_locator(mdates.DayLocator(interval=730))
		#	x = matplotlib.dates.datestr2num(self.date_parser(list(self.df['Date'])))
		x = list(self.df['Date'])
		x_as_dates = self.date_parser(x)
		X = mdates.datestr2num(x_as_dates)
		#x = mdates.datestr2num(self.date_parser(x))
		plt.xlabel('Time Period')
		plt.plot(X, y, color='blue', label='Original dataset')
		#plt.plot(timeseries, color='blue', label='USD')
		
		#	print x[:20]
		#	sys.exit()

		#rolmean = timeseries.rolling(1, min_periods=1).mean()
		#plt.plot(rolmean, color='blue', label='monthly_mean')
		
		model = ARIMA(y, order=(2, 1, 1))
		results_AR = model.fit(disp=-1)

		y = [float(x) for x in list(results_AR.fittedvalues)]
		y[-1] = init_val
		for i in range(len(y)-2, -1,-1):
			y[i] += y[i+1]
		#	print 'lengths x, y:', len(X), len(y), '\n\n'
		y = [y[-1]]+y[::-1]
		y = y[::-1]
		print 'init:',y[:10], ', final:', y[-10:] 
		for t in range(1):
			model = ARIMA(y, order=(2,1,1))
			model_fit = model.fit(disp=0)
			output = model_fit.forecast()
			yhat = output[0]
			pred=float(yhat)
			print 'yhat=', yhat
			
			#	obs = test[t]
			#	history.append(obs)
			#	print('predicted=%f, expected=%f' % (np.exp(yhat), np.exp(obs))
		sys.exit()
		
		plt.plot(X, y, color='red', label='Prediction dataset')
		plt.title('USD vs INR')
		plt.gcf().autofmt_xdate()
		plt.legend(loc='upper left')
		plt.show()
		
		
	def get_dates_and_prices(self, df):
		
		
		
	def predictor(self, value):
		y = list(self.df['Price'])
		y.reverse()
		x = range(0, len(y))
		x = np.asarray(x)
		y = np.asarray(y)
		clf = LinearRegression()
		clf.fit(x.reshape(-1, 1), y)
		return clf.predict(value)
		
	def date_parser(self, date_list):
		months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
		new_date_list = list()	
		for x in date_list:
			month, day, year = x.split(' ')
			day = day.strip(',')
			month = months.index(month)+1
			day, year = int(day), int(year)
			formatted_date = str(dt.datetime(year, month, day)).split(' ')[0]
			new_date_list.append(formatted_date)
		return new_date_list
